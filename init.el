;; init.el -- tangled from config.org -*- lexical-binding: t; -*-

(defconst my-emacs26 (version< emacs-version "27.0"))
(defconst my-emacs27 (not (version< emacs-version "27.0")))

(defvar orig-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(when my-emacs26
  (package-initialize)
  (setq package-enable-at-startup nil))

(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)
(setq use-package-always-ensure t
      use-package-expand-minimally t)

(use-package benchmark-init
  :demand
  :config
  ;; To disable collection of benchmark data after init is done.
  (add-hook 'after-init-hook 'benchmark-init/deactivate))

(when my-emacs26
  (load-file (expand-file-name "early-init.el" user-emacs-directory)))

(add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))

(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-auto-revert-mode +1)
(global-hl-line-mode +1)
(column-number-mode +1)

(setq make-backup-files nil           ; stop creating backup~ files
      auto-save-default nil           ; stop creating #autosave# files
      auto-save-list-file-prefix nil  ; stop creating auto-save-list dir
      create-lockfiles nil
      insert-directory-program "/usr/local/bin/gls"
      scroll-conservatively 1001
      scroll-preserve-screen-position t
      scroll-margin 3
      blink-cursor-blinks 0
      ring-bell-function #'ignore
      use-dialog-box nil
      auto-window-vscroll nil
      confirm-kill-processes nil
      delete-by-moving-to-trash t
      trash-directory "~/.Trash"
      uniquify-buffer-name-style 'forward
      pop-up-windows nil)               ; prevent split window
(setq-default cursor-type '(bar . 2)
              ad-redefinition-action 'accept
              indent-tabs-mode nil
              bidi-display-reordering nil
              ;;inhibit-startup-message t
              inhibit-default-init t
              split-width-threshold 160
              split-height-threshold nil
              initial-major-mode 'fundamental-mode)

(fset #'yes-or-no-p #'y-or-n-p)

(defmacro run-lazy (&rest args)
  "Run args later"
  (declare (indent defun))
  `(run-with-idle-timer 1 nil (lambda () ,@args)))

(defun my-local (file)
  "Return FILE path in local directory."
  (concat user-emacs-directory "local/" (system-name) "/" file))

(setq custom-file (my-local "custom.el"))
(load custom-file 'noerror)

(when (memq window-system '(mac ns))
  (use-package exec-path-from-shell
    :defer 1
    :config
    (exec-path-from-shell-initialize))

  (use-package ns-auto-titlebar
    :hook (window-setup . ns-auto-titlebar-mode))

  (setq mac-command-modifier 'super
        mac-option-modifier 'meta))

(use-package recentf
  :ensure nil
  :defer 1
  :hook (kill-emacs . recentf-cleanup)
  :config
  (setq recentf-auto-cleanup 'never
        recentf-max-menu-items 0
        recentf-max-saved-items 1000
        recentf-save-file (my-local "recentf")
        recentf-exclude '("/\\.cache/\\|/bookmarks"))
  (recentf-mode 1))

(use-package saveplace
  :ensure nil
  :defer 1
  :config
  (setq save-place-file (my-local "places"))
  (save-place-mode +1))

(use-package savehist
  :ensure nil
  :defer 1
  :config
  (setq savehist-file (my-local "savehist-history"))
  (savehist-mode +1))

(use-package frame
  :ensure nil
  :config
  (defun my-save ()
    (save-some-buffers t))

  (with-no-warnings
    (if my-emacs26
        (add-hook 'focus-out-hook #'my-save)
      (add-function :after after-focus-change-function #'my-save))))

(use-package save-frame
  :ensure nil)

(use-package eshell
  :ensure nil
  :bind
  ("s-e" . eshell))

(use-package esh-autosuggest
  :hook (eshell-mode . esh-autosuggest-mode))

(use-package compile
  :ensure nil
  :preface
  (defun compilation-ansi-color-process-output ()
    (ansi-color-process-output nil)
    (set (make-local-variable 'comint-last-output-start)
         (point-marker)))
  :hook (compilation-filter . compilation-ansi-color-process-output))

(use-package dired
  :ensure nil
  :bind
  (:map dired-mode-map
        ("e" . dired-previous-line))
  :hook
  (dired-mode . (lambda () (evil-snipe-mode -1)))
  :custom
  (dired-auto-revert-buffer t))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package subword
  :ensure nil
  :hook (prog-mode . subword-mode))

(use-package 'bookmark
  :ensure nil
  :defer t
  :custom
  (bookmark-file (my-local "bookmarks")))

(set-face-attribute 'default nil :font "Operator Mono" :height 140 :weight 'light)
(setq-default line-spacing 1)

(use-package pretty-code
  :ensure nil)

(defun set-pretty-symbols (args)
  "Set pretty symbols. ARG is ((string . char) (string . char) ...)"
  (dolist (pair args)
    (push pair prettify-symbols-alist))
  (when prettify-symbols-mode
    (prettify-symbols-mode -1))
  (prettify-symbols-mode 1))

(when my-emacs27
  (use-package display-fill-column-indicator
    :ensure nil
    :hook ((prog-mode text-mode) . display-fill-column-indicator-mode)
    :config
    (setq-default fill-column 100)))

(defun my-fill-column-indicator ()
  "Set fill column indicator foreground depending on theme."
  (when my-emacs27
    (let ((foreground (if (eq (frame-parameter nil 'background-mode) 'light)
                          "#ddd"
                        "#444")))
      (set-face-attribute 'fill-column-indicator nil :foreground foreground))))

(use-package monokai-theme :defer t)
(use-package doom-themes :defer t
  :custom
  (doom-themes-treemacs-theme "doom-colors"))
(use-package color-theme-sanityinc-tomorrow :defer t)
(use-package leuven-theme :defer t)
(use-package gruvbox-theme :defer t)
(use-package dracula-theme :defer t)
(use-package warm-night-theme :defer t)
(use-package zerodark-theme :defer t)

(defun my-random-theme ()
  "Choose random theme"
  (interactive)
  (let* ((themes '(
                   doom-one
                   doom-nord
                   doom-nova
                   doom-opera
                   doom-snazzy
                   doom-Iosvkem
                   doom-dracula
                   doom-gruvbox
                   doom-molokai
                   doom-peacock
                   doom-vibrant
                   doom-moonlight
                   doom-one-light
                   doom-palenight
                   doom-sourcerer
                   doom-spacegrey
                   doom-nord-light
                   doom-city-lights
                   doom-fairy-floss
                   doom-opera-light
                   doom-wilmersdorf
                   doom-tomorrow-day
                   doom-solarized-dark
                   doom-tomorrow-night
                   doom-challenger-deep
                   doom-outrun-electric
                   doom-solarized-light
                   monokai sanityinc-tomorrow-eighties leuven gruvbox-dark-medium dracula warm-night
                   ;; zerodark
                   ))
         (n (random (length themes)))
         (theme (nth n themes)))
    (dolist (enabled-theme custom-enabled-themes)
      (disable-theme enabled-theme))
    (load-theme theme t)
    (when nil
      (setq-default face-remapping-alist '((default :stipple "alpha:10%"))))
    (unless (memq theme '(sanityinc-tomorrow-eighties))
      (my-fill-column-indicator))
    (doom-themes-treemacs-config)
    (message "%s" theme)))

(my-random-theme)

(use-package startup
  :ensure nil
  :no-require
  :hook (window-setup . my-setup)
  :bind
  ("s-T" . my-random-theme)
  ("s-q" . save-buffers-kill-terminal)
  ("s-s" . save-buffer)
  ("s-c" . kill-ring-save)
  ("s-k" . kill-this-buffer)
  ("s-v" . yank)
  ("s-x" . kill-region)
  :preface
  (defun my-setup ()
    "Set up window."
    ;; (doom-modeline-mode 1)
    ;; (my-layout-3)
    (setq file-name-handler-alist orig-file-name-handler-alist)
    (setq-default gc-cons-threshold 16777216)))

(use-package doom-modeline
  :custom
  (doom-modeline-percent-position nil)
  :config
  (doom-modeline-def-modeline 'main
    '(bar workspace-name window-number matches buffer-info remote-host buffer-position parrot selection-info)
    '(objed-state misc-info persp-name fancy-battery grip irc mu4e github debug lsp minor-modes input-method indent-info buffer-encoding major-mode process vcs checker))

  (run-lazy
    (set-fontset-font t '(#Xe161 . #Xe162) "Material Icons" nil 'prepend))
  (doom-modeline-mode +1))

(use-package anzu
  :after evil
  :bind
  ([remap query-replace] . anzu-query-replace-regexp)
  :config
  (global-anzu-mode 1))

(use-package evil-anzu
  :after (evil anzu))

(use-package centaur-tabs
  :disabled
  :hook (after-init . centaur-tabs-mode)
  :bind
  ("s-<left>" . centaur-tabs-backward-tab)
  ("s-<right>" . centaur-tabs-forward-tab)
  :config
  (setq centaur-tabs-height 20
        centaur-tabs-set-bar 'left
        centaur-tabs-set-modified-marker t
        centaur-tabs-set-icons t
        centaur-tabs-modified-marker "●"
        centaur-tabs-gray-out-icons 'buffer))

(use-package display-line-numbers
  :ensure nil
  :hook ((prog-mode text-mode) . display-line-numbers-mode)
  :config
  (set-face-attribute 'line-number nil :height 0.9 :weight 'light)
  (set-face-attribute 'line-number-current-line nil :height 0.9 :weight 'light :inherit 'default)
  (setq-default display-line-numbers-width 4))

(use-package whitespace
  :ensure nil
  :hook
  (after-change-major-mode . doom|highlight-non-default-indentation)
  ((prog-mode text-mode conf-mode) . my-show-trailing-whitespace)
  :preface
  (defun doom|highlight-non-default-indentation ()
    "Highlight whitespace that doesn't match your `indent-tabs-mode' setting.

e.g. If you indent with spaces by default, tabs will be highlighted. If you
indent with tabs, spaces at BOL are highlighted.

Does nothing if `whitespace-mode' is already active or the current buffer is
read-only or not file-visiting."
    (unless (or (eq major-mode 'fundamental-mode)
                buffer-read-only
                (null buffer-file-name))
      (require 'whitespace)
      (set (make-local-variable 'whitespace-style)
           `(face ,@(if indent-tabs-mode '(indentation) '(tabs tab-mark))))
      (whitespace-mode +1)))

  (defun my-show-trailing-whitespace ()
    (setq show-trailing-whitespace t)))

(use-package all-the-icons
  :defer t)

(use-package dashboard
  :disabled
  :config
  (setq dashboard-items '())
  (dashboard-insert-startupify-lists)
  (switch-to-buffer "*dashboard*")
  (goto-char (point-min)))

(use-package paradox
  :bind
  (("s-L" . paradox-list-packages)
   :map paradox-menu-mode-map
   ("e" . paradox-previous-entry))
  :custom
  (paradox-github-token t)
  (paradox-execute-asynchronously nil)
  :config
  (evil-set-initial-state 'paradox-commit-list-mode 'emacs))

(use-package treemacs
  :after evil
  :bind
  (("s-t" . treemacs-select-window)
   :map treemacs-mode-map
   ("e" . treemacs-previous-line))
  :config
  (setq treemacs-persist-file (my-local "treemacs-persist"))
  (evil-set-initial-state 'treemacs-mode 'emacs)
  ;; (treemacs-resize-icons 16)
  )

(use-package winum
  :bind
  ("s-1" . winum-select-window-1)
  ("s-2" . winum-select-window-2)
  ("s-3" . winum-select-window-3)
  ("s-4" . winum-select-window-4)
  ("s-5" . winum-select-window-5)
  ("s-6" . winum-select-window-6)
  ("s-I" . my-layout)
  ("s-i" . my-layout-3)
  ("s-d" . my-duplicate-buffer)

  :commands (my-layout-3)
  :config
  (defun my-layout-3 ()
    "Setup window layout. 4 windows in grid."
    (interactive)
    (treemacs-select-window)
    (winum-select-window-1)
    (while (> (count-windows) 2)
      (delete-window))
    (split-window-right)
    (windmove-right)
    (split-window-below)
    (windmove-left)
    (message "%s" custom-enabled-themes))

  (defun my-layout ()
    "Setup window layout. 4 windows in grid."
    (interactive)
    (treemacs-select-window)
    (winum-select-window-1)
    (while (> (count-windows) 2)
      (delete-window))
    (split-window-below)
    (split-window-right)
    (windmove-down)
    (split-window-right)
    (windmove-up)
    (message "%s" custom-enabled-themes))

  (defun my-duplicate-buffer ()
    "Show current buffer in the other window."
    (interactive)
    (let* ((buf (current-buffer))
           (next-win (1+ (winum-get-number)))
           (max-win (1- (count-windows)))
           (correct-win (if (> next-win max-win) 1 next-win)))
      (winum-select-window-by-number correct-win)
      (switch-to-buffer buf nil t)))

  (winum-mode 1))

(run-lazy
  ;; (require 'my-helm)
  (require 'my-ivy)
  ;; (require 'my-ido)
  ;; (require 'my-snails)
  (require 'fix-korean))

(use-package which-key
  :disabled
  :defer 1
  :config
  (which-key-mode 1))

(use-package which-key-posframe
  :disabled
  :after (which-key doom-modeline)
  :config
  (defun my-poshandler (info)
    "My posframe's position handler.

Get a position which let posframe stay onto its parent-frame's
bottom left corner.  The structure of INFO can be found
in docstring of `posframe-show'."
    (cons (/ (- (plist-get info :parent-frame-width)
                (plist-get info :posframe-width))
             2)
          (- 0
             (plist-get info :mode-line-height)
             (plist-get info :minibuffer-height))))
  (setq which-key-posframe-poshandler #'my-poshandler)
  (which-key-posframe-mode 1))

(use-package helpful
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-symbol] . helpful-at-point)
  ([remap describe-key] . helpful-key)
  ([remap describe-variable] . helpful-variable)
  :config
  (evil-define-key* 'normal helpful-mode-map "q" 'quit-window))

(use-package editorconfig
  :defer 1
  :config
  (editorconfig-mode +1))

(use-package highlight-parentheses
  :hook (prog-mode . highlight-parentheses-mode)
  :config
  (setq hl-paren-colors '("Springgreen3"
                          "IndianRed1"
                          "IndianRed3"
                          "IndianRed4"))
  (setq hl-paren-delay 0.5)
  (set-face-attribute 'hl-paren-face nil :weight 'ultra-bold))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode)
  :config
  (setq rainbow-delimiters-max-face-count 5))

(use-package smartparens
  :hook (prog-mode . smartparens-mode)
  :bind
  ("M-<backspace>" . sp-unwrap-sexp)
  ("M-<left>" . sp-forward-barf-sexp)
  ("M-<right>" . sp-forward-slurp-sexp)
  :config
  (require 'smartparens-config)
  (when my-emacs27
    (dolist (fun '(c-electric-paren c-electric-brace))
      (add-to-list 'sp--special-self-insert-commands fun)))

  (setq sp-escape-quotes-after-insert nil)

  (sp-local-pair 'elixir-mode "do" "end"
                 :when '(("RET" "<evil-ret>"))
                 :unless '(sp-in-comment-p sp-in-string-p)
                 :post-handlers '("||\n[i]"))
  (sp-local-pair 'csharp-mode "{" nil
                 :post-handlers '(("||\n[i]" "RET") ("| " "SPC"))
                 :unless '(sp-point-before-word-p sp-point-before-same-p)))

(use-package aggressive-indent
  :hook
  (emacs-lisp-mode . aggressive-indent-mode))

(use-package company
  :hook (prog-mode . company-mode)
  :config
  (setq company-minimum-prefix-length 1))

(use-package prescient
  :after ivy
  :config
  (setq prescient-save-file (my-local "prescient-save.el"))
  (prescient-persist-mode +1))

(use-package company-prescient
  :after company
  :config
  (company-prescient-mode 1))

(use-package company-box
  :hook (company-mode . company-box-mode)
  :config
  (setq company-box-show-single-candidate t
        company-box-backends-colors nil
        company-box-max-candidates 50
        company-box-icons-alist 'company-box-icons-all-the-icons
        company-box-icons-functions
        '(+company-box-icons--yasnippet company-box-icons--lsp +company-box-icons--elisp company-box-icons--acphp)
        company-box-icons-all-the-icons
        `((Unknown       . ,(all-the-icons-material "find_in_page"             :height 0.8 :face 'all-the-icons-purple))
          (Text          . ,(all-the-icons-material "text_fields"              :height 0.8 :face 'all-the-icons-green))
          (Method        . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Function      . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Constructor   . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Field         . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Variable      . ,(all-the-icons-material "adjust"                   :height 0.8 :face 'all-the-icons-blue))
          (Class         . ,(all-the-icons-material "class"                    :height 0.8 :face 'all-the-icons-red))
          (Interface     . ,(all-the-icons-material "settings_input_component" :height 0.8 :face 'all-the-icons-red))
          (Module        . ,(all-the-icons-material "view_module"              :height 0.8 :face 'all-the-icons-red))
          (Property      . ,(all-the-icons-material "settings"                 :height 0.8 :face 'all-the-icons-red))
          (Unit          . ,(all-the-icons-material "straighten"               :height 0.8 :face 'all-the-icons-red))
          (Value         . ,(all-the-icons-material "filter_1"                 :height 0.8 :face 'all-the-icons-red))
          (Enum          . ,(all-the-icons-material "plus_one"                 :height 0.8 :face 'all-the-icons-red))
          (Keyword       . ,(all-the-icons-material "filter_center_focus"      :height 0.8 :face 'all-the-icons-red))
          (Snippet       . ,(all-the-icons-material "short_text"               :height 0.8 :face 'all-the-icons-red))
          (Color         . ,(all-the-icons-material "color_lens"               :height 0.8 :face 'all-the-icons-red))
          (File          . ,(all-the-icons-material "insert_drive_file"        :height 0.8 :face 'all-the-icons-red))
          (Reference     . ,(all-the-icons-material "collections_bookmark"     :height 0.8 :face 'all-the-icons-red))
          (Folder        . ,(all-the-icons-material "folder"                   :height 0.8 :face 'all-the-icons-red))
          (EnumMember    . ,(all-the-icons-material "people"                   :height 0.8 :face 'all-the-icons-red))
          (Constant      . ,(all-the-icons-material "pause_circle_filled"      :height 0.8 :face 'all-the-icons-red))
          (Struct        . ,(all-the-icons-material "streetview"               :height 0.8 :face 'all-the-icons-red))
          (Event         . ,(all-the-icons-material "event"                    :height 0.8 :face 'all-the-icons-red))
          (Operator      . ,(all-the-icons-material "control_point"            :height 0.8 :face 'all-the-icons-red))
          (TypeParameter . ,(all-the-icons-material "class"                    :height 0.8 :face 'all-the-icons-red))
          ;; (Template   . ,(company-box-icons-image "Template.png"))))
          (Yasnippet     . ,(all-the-icons-material "short_text"               :height 0.8 :face 'all-the-icons-green))
          (ElispFunction . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (ElispVariable . ,(all-the-icons-material "check_circle"             :height 0.8 :face 'all-the-icons-blue))
          (ElispFeature  . ,(all-the-icons-material "stars"                    :height 0.8 :face 'all-the-icons-orange))
          (ElispFace     . ,(all-the-icons-material "format_paint"             :height 0.8 :face 'all-the-icons-pink))))

  (defun +company-box-icons--yasnippet (candidate)
    (when (get-text-property 0 'yas-annotation candidate)
      'Yasnippet))

  (defun +company-box-icons--elisp (candidate)
    (when (derived-mode-p 'emacs-lisp-mode)
      (let ((sym (intern candidate)))
        (cond ((fboundp sym)  'ElispFunction)
              ((boundp sym)   'ElispVariable)
              ((featurep sym) 'ElispFeature)
              ((facep sym)    'ElispFace))))))

  (use-package yasnippet
    :hook ((text-mode prog-mode conf-mode snippet-mode) . yas-minor-mode-on)
    :config
    (yas-reload-all))

(use-package evil
  :defer 1
  :config
  (defun my-escape ()
    "Run evil-normal-state and switch to English IME."
    (interactive)
    (evil-normal-state)
    (shell-command "~/.doom.d/bin/xkbswitch -s 5"))

  (defun my-korean-insert ()
    "Run evil-insert-state and switch to Korean IME."
    (interactive)
    (evil-insert-state 1)
    (shell-command "~/.doom.d/bin/xkbswitch -s 3"))

  (evil-define-key* '(motion) 'global
                    "n" 'evil-next-line
                    "e" 'evil-previous-line
                    "i" 'evil-forward-char
                    "l" 'evil-forward-word-end
                    "[e" 'previous-error
                    "]e" 'next-error)
  (evil-define-key* '(normal) 'global
                    "i" nil
                    "k" 'evil-insert
                    "K" 'my-korean-insert
                    "j" 'evil-search-next)
  (evil-define-key* '(visual operator) 'global
                    "i" nil
                    "k" evil-inner-text-objects-map)
  (evil-define-key* '(insert) 'global
                    [escape] 'my-escape)
  (evil-mode +1))

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package avy
  :defer t
  :config
  (setq avy-keys '(?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9 ?0)))

(use-package evil-easymotion
  :after evil
  :bind
  (:map evilem-map
        ("SPC" . #'avy-goto-char-timer)
        ("n" . #'evilem-motion-next-line)
        ("e" . #'evilem-motion-previous-line)
        ("j" . #'evilem-motion-search-next)
        ("l" . #'evilem-motion-forward-word-end))
  :config
  (evilem-default-keybindings "SPC"))

(use-package evil-snipe
  :after evil
  :config
  (evil-snipe-mode +1)
  (evil-snipe-override-mode +1))

(use-package multiple-cursors
  :commands (swiper-mc)
  :config
  (setq mc/list-file (my-local ".mc-lists.el"))
  (push 'swiper-mc mc/cmds-to-run-once))

(use-package evil-mc
  :disabled
  :after evil
  :hook (evil-insert-state-entry . evil-mc-resume-cursors)
  :config
  (global-evil-mc-mode 1))

(use-package evil-multiedit
  :after evil
  :config
  (evil-multiedit-default-keybinds))

(use-package undo-tree
  :bind
  ("s-z" . undo-tree-undo)
  ("s-Z" . undo-tree-visualize)
  :config
  (let ((history-dir (my-local "undo-history")))
    (make-directory history-dir t)
    (setq undo-tree-auto-save-history t
          undo-tree-history-directory-alist `(("." . ,history-dir)))))

(use-package outshine
  :disabled
  :hook (emacs-lisp-mode . outshine-mode)
  ;; :custom-face
  ;; (outshine-level-1 ((t (:height 1.25))))
  ;; (outshine-level-2 ((t (:height 1.2))))
  ;; (outshine-level-3 ((t (:height 1.15))))
  ;; (outshine-level-4 ((t (:height 1.1))))
  ;; (outshine-level-5 ((t (:height 1.05))))
  :config
  (setq outshine-use-speed-commands t))

(use-package pretty-outlines
  :disabled
  :ensure nil
  :after outshine
  :hook (;; (outline-mode       . pretty-outlines-set-display-table)
         ;; (outline-minor-mode . pretty-outlines-set-display-table)
         (outshine-mode . pretty-outlines-add-bullets)))

(use-package magit
  :bind
  ("s-G" . magit-status)
  :config
  (setq transient-history-file (my-local "transient-history.el")))

(use-package git-gutter-fringe
  :preface
  (defun enable-git-gutter-fringe ()
    (require 'git-gutter-fringe)
    (run-at-time 1 nil 'git-gutter-mode 1))

  :hook ((prog-mode text-mode conf-mode) . enable-git-gutter-fringe)
  :config
  (add-function :after after-focus-change-function #'git-gutter:update-all-windows)
  (define-fringe-bitmap 'git-gutter-fr:added [224]
    nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [#x60 #x30 #x18 #x30]
    nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240]
    nil nil 'bottom))

(server-start)

(use-package prog-mode
  :ensure nil
  :custom
  (prettify-symbols-unprettify-at-point 'right-edge))

(use-package lsp-mode
  :hook ((web-mode elixir-mode typescript-mode) . lsp)
  ;; :hook (before-save . lsp-format-buffer)
  :config
  (setq lsp-session-file (my-local ".lsp-session-v1")))

(use-package lsp-ui
  :defer t
  :config
  (setq lsp-prefer-flymake nil
        lsp-enable-symbol-highlighting nil
        lsp-ui-doc-enable nil
        lsp-ui-sideline-enable nil))

(use-package company-lsp
  :defer t)

(use-package lsp-treemacs
  :defer t)

(use-package projectile
  :commands (projectile-project-root
             projectile-project-name
             projectile-project-p
             projectile-add-known-project) ; TODO PR autoload upstream
  :config
  (setq projectile-known-projects-file (my-local "projectile-bookmarks.eld")
        projectile-cache-file (my-local "projectile.cache"))
  (projectile-mode 1))

(use-package flycheck
  :defer t)

(use-package flycheck-posframe
  :hook (flycheck-mode . flycheck-posframe-mode))

(use-package web-mode
  :mode "\\.vue\\'"
  :hook (web-mode . my-web-mode)
  :config
  (defun my-web-mode ()
    (setq web-mode-script-padding 0))
  (setq lsp-vetur-format-default-formatter-options
        '((js-beautify-html
           (wrap_attributes . "force-expand-multiline"))
          (prettyhtml
           (printWidth . 120)
           (singleQuote . :json-false)
           (wrapAttributes . :json-false)
           (sortAttributes . :json-false))
          (prettier
           (printWidth . 120))))
  )

(use-package elixir-mode
  :bind
  (:map elixir-mode-map
        ("<f9>" . my-mix-test)
        ("S-<f9>" . my-mix-test-all)
        ("<f12>" . lsp-ui-peek-find-definitions))
  :hook (elixir-mode . my-elixir-mode)
  :config
  (defun my-elixir-mode ()
    "Set up elixir mode."
    (set-pretty-symbols '(("&&" . ?∧)
                          ("||" . ?∨)
                          ("!" . ?￢)
                          ("for" . ?∀)
                          ("nil" . ?∅)
                          ("fn" . ?λ)
                          ("def" . ?ƒ)
                          ("true" . ?𝕋)
                          ("false" . ?𝔽)
                          ("integer" . ?ℤ)
                          ("float" . ?ℝ)
                          ("string" . ?𝕊)
                          ("boolean" . ?𝔹)))
    (add-hook 'after-save-hook 'my-mix-format nil t))

  (defun my-mix-test ()
    "Run mix test with current buffer."
    (interactive)
    (save-some-buffers t)
    (let ((default-directory (concat default-directory "..")))
      (compile (concat "mix copy; MIX_ENV=test mix test " (buffer-file-name)))))

  (defun my-mix-test-all ()
    "Run mix test."
    (interactive)
    (save-some-buffers t)
    (let ((default-directory "~/work/q5/program/server/apps/world_server"))
      (compile "mix copy; MIX_ENV=test mix test")))

  (defun my-mix-format ()
    "Run mix format"
    (let ((default-directory "~/work/q5/program/server"))
      (shell-command (concat "mix format " (shell-quote-argument (buffer-file-name))))))

  (setq lsp-clients-elixir-server-executable "~/.vscode/extensions/jakebecker.elixir-ls-0.2.25/elixir-ls-release/language_server.sh"))

(use-package typescript-mode
  :mode "\\.ts\\'"
  :config
  (setq lsp-typescript-npm "/usr/local/bin/npm"))

(use-package json-mode
  :mode "\\.json\\'")

(use-package yaml-mode
  :mode "\\.yml\\'")

(use-package csv-mode
  :mode "\\.csv\\'"
  :config
  (custom-set-variables '(csv-separators '("|"))))

(use-package org
  :ensure org-plus-contrib
  :hook
  (org-mode . my-org-mode)
  :bind
  (("s-C" . counsel-org-capture)
   :map org-mode-map
   ("<f9>" . my-compile-init)
   ("s-l" . org-toggle-link-display)
   ("s-'" . org-edit-special)
   :map org-src-mode-map
   ("s-'" . org-edit-src-exit))
  :custom
  (org-export-with-sub-superscripts '{})
  :config
  (defun my-org-mode ()
    (org-indent-mode 1)
    (org-bullets-mode 1)
    (set-pretty-symbols '(("#+BEGIN_SRC" . ?»)
                          ("#+END_SRC" . ?«))))

  (defun my-compile-init ()
    "Tangle config.org file."
    (interactive)
    (let ((init-file (expand-file-name "init.el" user-emacs-directory))
          (config-file (file-truename (expand-file-name "config.org" user-emacs-directory)))
          (buffer-file (file-truename buffer-file-name)))
      (when (and (eq major-mode 'org-mode)
                 (equal config-file buffer-file))
        (save-buffer)
        (org-babel-tangle-file buffer-file-name init-file "emacs-lisp"))))

  (setq org-src-preserve-indentation t
        org-src-window-setup 'other-window))

(use-package org-bullets
  :defer t)

(use-package htmlize
  :after org)

(use-package csharp-mode
  :mode "\\.cs\\'"
  :hook (csharp-mode . my-csharp-mode)
  :config
  (defun my-csharp-mode ()
    (c-set-style "c#")
    (omnisharp-mode 1)
    (flycheck-mode 1)
    (set-pretty-symbols '(("&&" . ?∧)
                          ("||" . ?∨)
                          ("!" . ?￢)
                          ("for" . ?∀)
                          ("foreach" . ?∀)
                          ("in" . ?∈)
                          ("null" . ?∅)
                          ("() =>" . ?λ)
                          ("true" . ?𝕋)
                          ("false" . ?𝔽)
                          ("int" . ?ℤ)
                          ("float" . ?ℝ)
                          ("string" . ?𝕊)
                          ("bool" . ?𝔹)))
    (add-hook 'before-save-hook 'omnisharp-code-format-entire-file nil t)))

(use-package omnisharp
  :bind
  (:map csharp-mode-map
        ("<f12>" . omnisharp-go-to-definition)
        ("S-<f12>" . omnisharp-find-usages))
  :hook
  (omnisharp-mode . (lambda () (yas-minor-mode -1)))
  :config
  ;;(setq omnisharp-server-executable-path "~/.emacs.d/.cache/omnisharp/server/v1.32.18/run")
  (setq omnisharp-server-executable-path "~/.vscode/extensions/ms-vscode.csharp-1.21.0/.omnisharp/1.34.0/run")
  (add-to-list 'company-backends #'company-omnisharp))

(use-package elisp-mode
  :ensure nil
  :bind
  ("s-o" . eval-last-sexp))
