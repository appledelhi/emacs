;; -*- lexical-binding: t; -*-

;;; Garbage collection
;; Defer garbage collection further back in the startup process
(setq gc-cons-threshold 268435456)

;;; Frame
;; Prevent the glimpse of un-styled Emacs by setting these early.
(add-to-list 'default-frame-alist '(tool-bar-lines . 0))
(add-to-list 'default-frame-alist '(menu-bar-lines . 0))
(add-to-list 'default-frame-alist '(vertical-scroll-bars))

;; One less file to load at startup
(setq site-run-file nil)
