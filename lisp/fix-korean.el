;; fix-korean.el --- Replace Koreantyped unintentionally with English.	-*- lexical-binding: t -*-

;; Copyright (C) 2019 Jake Song

;; Author: Jake Song <jksong3@gmail.com>
;; URL:

;; This file is not part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;

;;; Commentary:
;;
;; Replace modifier + Korean letters with modifier + English letters at same keyboard location.
;; And change input method to English mode.
;;

;;; Code:

(defun my-set-key (hangul english)
  (global-set-key (kbd hangul) (lambda ()
                                 (interactive)
                                 (shell-command "~/.doom.d/bin/xkbswitch -s 5")
                                 (setq unread-command-events (listify-key-sequence (kbd english))))))

(defun my-set-keys (alist)
  "Map Control-Hangul key to change input mode to English and intended control key."
  (mapc (lambda (pair)
          (let ((hangul (car pair))
                (english (cdr pair)))
            (my-set-key hangul english)))
        alist))

(my-set-keys '(("C-ㅁ" . "C-a")
               ("C-ㅠ" . "C-b")
               ("C-ㅊ" . "C-c")
               ("C-ㅎ" . "C-d")
               ("C-ㅏ" . "C-e")
               ("C-ㄷ" . "C-f")
               ("C-ㅅ" . "C-g")
               ("C-ㅗ" . "C-h")
               ("C-ㅣ" . "C-i")
               ("C-ㅛ" . "C-j")
               ("C-ㅜ" . "C-k")
               ("C-ㅕ" . "C-l")
               ("C-ㅡ" . "C-m")
               ("C-ㅓ" . "C-n")
               ("C-ㄱ" . "C-p")
               ("C-ㅂ" . "C-q")
               ("C-ㄴ" . "C-r")
               ("C-ㅇ" . "C-s")
               ("C-ㄹ" . "C-t")
               ("C-ㅑ" . "C-u")
               ("C-ㅍ" . "C-v")
               ("C-ㅈ" . "C-w")
               ("C-ㅌ" . "C-x")
               ("C-ㅐ" . "C-y")
               ("C-ㅋ" . "C-z")
               ("M-ㅁ" . "M-a")
               ("M-ㅠ" . "M-b")
               ("M-ㅊ" . "M-c")
               ("M-ㅎ" . "M-d")
               ("M-ㅏ" . "M-e")
               ("M-ㄷ" . "M-f")
               ("M-ㅅ" . "M-g")
               ("M-ㅗ" . "M-h")
               ("M-ㅣ" . "M-i")
               ("M-ㅛ" . "M-j")
               ("M-ㅜ" . "M-k")
               ("M-ㅕ" . "M-l")
               ("M-ㅡ" . "M-m")
               ("M-ㅓ" . "M-n")
               ("M-ㄱ" . "M-p")
               ("M-ㅂ" . "M-q")
               ("M-ㄴ" . "M-r")
               ("M-ㅇ" . "M-s")
               ("M-ㄹ" . "M-t")
               ("M-ㅑ" . "M-u")
               ("M-ㅍ" . "M-v")
               ("M-ㅈ" . "M-w")
               ("M-ㅌ" . "M-x")
               ("M-ㅐ" . "M-y")
               ("M-ㅋ" . "M-z")
               ("s-ㅁ" . "s-a")
               ("s-ㅠ" . "s-b")
               ("s-ㅊ" . "s-c")
               ("s-ㅎ" . "s-d")
               ("s-ㅏ" . "s-e")
               ("s-ㄷ" . "s-f")
               ("s-ㅅ" . "s-g")
               ("s-ㅗ" . "s-h")
               ("s-ㅣ" . "s-i")
               ("s-ㅛ" . "s-j")
               ("s-ㅜ" . "s-k")
               ("s-ㅕ" . "s-l")
               ("s-ㅡ" . "s-m")
               ("s-ㅓ" . "s-n")
               ("s-ㄱ" . "s-p")
               ("s-ㅂ" . "s-q")
               ("s-ㄴ" . "s-r")
               ("s-ㅇ" . "s-s")
               ("s-ㄹ" . "s-t")
               ("s-ㅑ" . "s-u")
               ("s-ㅍ" . "s-v")
               ("s-ㅈ" . "s-w")
               ("s-ㅌ" . "s-x")
               ("s-ㅐ" . "s-y")
               ("s-ㅋ" . "s-z")
               ("s-ㅆ" . "s-G")))

(provide 'fix-korean)

;;; fix-korean.el ends here
