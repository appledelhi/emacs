(add-to-list 'load-path "~/.emacs.d/snails")

(use-package snails
  :bind
  (("s-w" . snails)
   ("s-/" . my-snails-rg)
   :map snails-mode-map
   ("<escape>" . snails-quit)
  )
  :config
  (defun my-snails-rg ()
    "Snails with rg"
    (interactive)
    (snails '(snails-backend-rg)))
  )

(provide 'my-snails)
