;; -*- lexical-binding: t -*-
;;; Helm

(use-package helm
  :bind
  (("M-x" . helm-M-x)
   ("s-r" . helm-resume)
   ("s-w" . helm-mini)
   :map helm-map
   ("<escape>" . helm-keyboard-quit))
  ;; :custom
  ;; (helm-display-function 'helm-display-buffer-in-own-frame)
  ;; (helm-display-buffer-width 90)
  ;; (helm-display-buffer-height 20)
  :config
  (require 'helm-config)
  (setq helm-split-window-inside-p t
        helm-echo-input-in-header-line t)
  (helm-mode 1))

;;;; helm posframe

;; (use-package helm-posframe
;;   :config
;;   (helm-posframe-enable))

;;;; helm-projectile

(use-package helm-projectile
  :bind
  ("s-/" . helm-projectile-ag)
  :config
  (setq helm-projectile-set-input-automatically nil)
  (helm-projectile-on))

;;;; helm-ag

(use-package helm-ag)

;;;; swiper-helm

(use-package swiper-helm
  :bind
  ("s-f" . swiper-helm)
  :config
  (setq swiper-helm-display-function 'helm-default-display-buffer))

(provide 'my-helm)
