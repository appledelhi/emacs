;; -*- lexical-binding: t; -*-
;;; Save frame

(defun my-save-frame ()
  "Gets the current frame's geometry and saves to ~/.emacs.d/frame.el."
  (let ((frameg-left (frame-parameter (selected-frame) 'left))
        (frameg-top (frame-parameter (selected-frame) 'top))
        (frameg-width (frame-text-width))
        (frameg-height (frame-text-height))
        (frameg-file (my-local "frame.el")))
    (with-temp-buffer
      (insert
       ";;; This file stores the previous emacs frame's geometry.\n"
       "(setq initial-frame-alist\n"
       (format "'((top . %d)\n" (max frameg-top 0))
       (format "  (left . %d)\n" (max frameg-left 0))
       (format "  (width . (text-pixels . %d))\n" (max frameg-width 0))
       (format "  (height . (text-pixels . %d))))\n" (max frameg-height 0)))
      (when (file-writable-p frameg-file)
        (write-file frameg-file)))))

;;; Load frame

(defun my-load-frame ()
  "Loads ~/.emacs.d/frame.el which should load the previous frame's geometry."
  (let ((frameg-file (my-local "frame.el")))
    (when (file-readable-p frameg-file)
      (load-file frameg-file))))

(my-load-frame)

(when (display-graphic-p)
  (add-hook 'kill-emacs-hook #'my-save-frame))

(provide 'save-frame)
