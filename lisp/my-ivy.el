;; -*- lexical-binding: t -*-
;;; Ivy

(use-package ivy
  :bind
  (("s-r" . ivy-resume)
   :map ivy-minibuffer-map
   ("<escape>" . minibuffer-keyboard-quit)
   ("M-<up>" . ivy-previous-line-and-call)
   ("M-<down>" . ivy-next-line-and-call))
  :config
;;  (advice-add 'ivy--minibuffer-setup :around 'ivy-posframe--minibuffer-setup)
  (defun ivy-display-function-window (text)
    (let ((buffer (get-buffer-create "*ivy-candidate-window*"))
          (str (with-current-buffer (get-buffer-create " *Minibuf-1*")
                 (let ((point (point))
                       (string (concat (buffer-string) "  " text)))
                   (ivy-add-face-text-property
                    (- point 1) point 'ivy-cursor string t)
                   string))))
      (with-current-buffer buffer
        (let ((inhibit-read-only t))
          (setq truncate-lines t)
          (erase-buffer)
          (insert str)
          (goto-char (point-min))))
      (with-ivy-window
        (display-buffer
         buffer
         `((display-buffer-reuse-window
            display-buffer-below-selected)
           (window-height . ,(1+ (ivy--height (ivy-state-caller ivy-last))))
           (window-parameters . ((header-line-format . none)
                                 (mode-line-format . none))))))))

  (setq ivy-use-virtual-buffers t
        ivy-height 15
        ivy-display-functions-alist '((swiper . ivy-display-function-window)
                                      (counsel-rg . ivy-display-function-window)
                                      (ivy-switch-buffer . ivy-display-function-window)
                                      (counsel-find-file . ivy-display-function-window)
                                      (t . ivy-display-function-window)))
  (ivy-mode 1))

;;;; Ivy prescient

(use-package ivy-prescient
  :defer 1
  :config
  (ivy-prescient-mode 1))

;;;; Ivy childframe

(use-package ivy-posframe
  :defer 1
  ;; :custom-face
  ;; (ivy-posframe-border ((t (:background "white"))))
  :config
  (defun ivy-posframe-display-at-window-bottom-right (str)
    (ivy-posframe--display str 'posframe-poshandler-window-bottom-right-corner))

  (setq ivy-posframe-min-width 100
        ivy-posframe-min-height 15
        ivy-posframe-border-width 2
        ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-bottom-left)))
  (ivy-posframe-mode 1))

;;;; Ivy rich

(use-package ivy-rich
  :defer 1
  :config
  (defun ivy-rich-switch-buffer-icon (candidate)
    (with-current-buffer
        (get-buffer candidate)
      (let ((icon (all-the-icons-icon-for-mode major-mode)))
        (if (symbolp icon)
            (all-the-icons-icon-for-mode 'fundamental-mode)
          icon))))

  (defun ivy-rich-file-icon (candidate)
    "Display file icons in `ivy-rich'."
    (when (display-graphic-p)
      (let* ((path (file-local-name (concat ivy--directory candidate)))
             (file (file-name-nondirectory path))
             (icon (cond
                    ((file-directory-p path)
                     (cond
                      ((and (fboundp 'tramp-tramp-file-p)
                            (tramp-tramp-file-p default-directory))
                       (all-the-icons-octicon "file-directory" :height 1.0 :v-adjust 0.01))
                      ((file-symlink-p path)
                       (all-the-icons-octicon "file-symlink-directory" :height 1.0 :v-adjust 0.01))
                      ((all-the-icons-dir-is-submodule path)
                       (all-the-icons-octicon "file-submodule" :height 1.0 :v-adjust 0.01))
                      ((file-exists-p (format "%s/.git" path))
                       (all-the-icons-octicon "repo" :height 1.1 :v-adjust 0.01))
                      (t (let ((matcher (all-the-icons-match-to-alist path all-the-icons-dir-icon-alist)))
                           (apply (car matcher) (list (cadr matcher) :v-adjust 0.01))))))
                    ((string-match "^/.*:$" path)
                     (all-the-icons-material "settings_remote" :height 1.0 :v-adjust -0.2))
                    ((not (string-empty-p file))
                     (all-the-icons-icon-for-file file :v-adjust -0.05)))))
        (if (symbolp icon)
            (all-the-icons-faicon "file-o" :face 'all-the-icons-dsilver :height 0.8 :v-adjust 0.0)
          icon))))

  (setq ivy-rich-display-transformers-list
        '(ivy-switch-buffer
          (:columns
           ((ivy-rich-switch-buffer-icon (:width 2))
            (ivy-rich-candidate (:width 30))
            (ivy-rich-switch-buffer-size (:width 7))
            (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
            (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
            (ivy-rich-switch-buffer-project (:width 15 :face success))
            (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
           :predicate
           (lambda (cand) (get-buffer cand)))
          counsel-find-file
          (:columns
           ((ivy-rich-file-icon (:width 2))
            (ivy-read-file-transformer)
            (ivy-rich-counsel-find-file-truename (:face font-lock-doc-face))))
          counsel-M-x
          (:columns
           ((counsel-M-x-transformer (:width 40))
            (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))
          counsel-describe-function
          (:columns
           ((counsel-describe-function-transformer (:width 40))
            (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))
          counsel-describe-variable
          (:columns
           ((counsel-describe-variable-transformer (:width 40))
            (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face))))
          counsel-recentf
          (:columns
           ((ivy-rich-candidate (:width 120))
            (ivy-rich-file-last-modified-time (:face font-lock-comment-face))))))
  (ivy-rich-mode 1))

;;;; Counsel

(use-package counsel
  :bind
  ("M-x" . counsel-M-x)
  ("C-x C-f" . counsel-find-file)
  ("s-w" . ivy-switch-buffer)
  ("s-b" . counsel-bookmark)
  ("s-R" . counsel-recentf)
  ("s-/" . counsel-projectile-rg)
  ("C-s-/" . my-counsel-projectile-rg)
  :config
  (defun my-counsel-projectile-rg ()
    "Run counsel-projectile-rg with thing at point."
    (interactive)
    (let ((counsel-projectile-rg-initial-input '(ivy-thing-at-point)))
      (counsel-projectile-rg)))

  (add-to-list 'ivy-more-chars-alist '(counsel-rg . 2))
  (add-to-list 'ivy-more-chars-alist '(counsel-projectile-rg . 2)))

;;;;;; Counsel projectile

(use-package counsel-projectile
  :bind
  ("s-p" . counsel-projectile)
  :config
  (setq counsel-find-file-ignore-regexp "\\.\\(prefab\\|meta\\|ogg\\|png\\|FBX\\|mat\\|anim\\)$"))

;;;; Swiper

(use-package swiper
  :bind
  ("s-f" . swiper)
  ("C-s-f" . swiper-thing-at-point))

(provide 'my-ivy)
